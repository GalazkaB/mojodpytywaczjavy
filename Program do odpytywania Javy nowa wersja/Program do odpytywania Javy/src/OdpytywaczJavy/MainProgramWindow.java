package OdpytywaczJavy;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.table.DefaultTableModel;
//najwazniejsze zmienne odpowiadające za logike programu, to:
// quePositionOnList - odpowiada pozycji pytania następnego po obecnie tym obecnie wyswietlonym
// currentWasDeleted - flaga, która mówi, czy pytanie obecnie wyświetlone zostało usunięte
// naciśnięcie 'start' w programie powoduje zainicjowanie quePositionOnList na wartosc 1.
public class MainProgramWindow {

	QuestionListWindow queListWindow;// okno listy pytan
	JFrame frame; // glowna ramka
	JPanel centerPane;
	JPanel southPane;
	JPanel northPane;
	JPanel westPane;
	JPanel eastPane;
	JTextArea queTextArea; // wyswietlone pytanie
	JLabel commentLabel; // napis komentarz
	JLabel modeLabel; // tryb

	JButton queListBtn;// otworz liste pytan
	JButton sectionRankBtn;// ranking dzialow
	JButton exitBtn; // wyjscie
	JButton losowoBtn;// tryb losowy
	JButton poKoleiBtn;// tryb poKolei

	JButton learnedBtn; // umiem
	JButton unlearnedBtn;// nie umiem
	JButton addCommentBtn;// dodaj komentarz
	JButton startBtn; // wyswietl pierwsze pytanie
	static int quePositionOnList;
	static boolean start;
	static boolean currentWasDeleted;


	public MainProgramWindow() {
		initMainWindowComponents(); //
		queListWindow = new QuestionListWindow();
	}

	public static void main(String[] args) {
		MainProgramWindow mPWindow = new MainProgramWindow();

	}

	public void initMainWindowComponents() {

		frame = new JFrame("Odpytywacz Javy"); // main frame

		// Main Frame Building
		centerPane = new JPanel();
		southPane = new JPanel();
		northPane = new JPanel();
		westPane = new JPanel();
		eastPane = new JPanel();

		queTextArea = new JTextArea("Kliknij " + "\"Start\"" + " by " + "rozpocząć naukę :)"); // pole na
																							// pytanie//cPane
		commentLabel = new JLabel("komentarz"); // komentarz obok pytania//cPane

		queTextArea.setFont(MyStyles.queFont);
		queTextArea.setForeground(MyStyles.queLettersColor);
		queTextArea.setBounds(100, 120, 688, 350);// kolor liter
		queTextArea.setBackground(MyStyles.ProgramBackgroundColor);
		queTextArea.setEditable(false);
		queTextArea.setLineWrap(true);

		learnedBtn = new JButton("UMIEM");// east
		unlearnedBtn = new JButton("NIE UMIEM");// west
		addCommentBtn = new JButton("Dodaj komentarz");// cPane

		queListBtn = new JButton("Lista pytań"); // sPane
		sectionRankBtn = new JButton("Ranking Znajomości Działów");// sPane
		exitBtn = new JButton("Wyjście");// sPane
		modeLabel = new JLabel("Tryb wyświetlania:");
		losowoBtn = new JButton("Losowo");
		poKoleiBtn = new JButton("Kolejno");
		startBtn = new JButton("Start");

		learnedBtn.setFont(MyStyles.buttonFont);// east
		learnedBtn.setPreferredSize(new Dimension(150, 150));
		unlearnedBtn.setFont(MyStyles.buttonFont);// west
		unlearnedBtn.setPreferredSize(new Dimension(150, 150));

		addCommentBtn.setFont(MyStyles.buttonFont);// cPane
		startBtn.setFont(MyStyles.buttonFont); // sPane
		queListBtn.setFont(MyStyles.buttonFont);// sPane
		sectionRankBtn.setFont(MyStyles.buttonFont);// sPane
		exitBtn.setFont(MyStyles.buttonFont);// sPane
		exitBtn.setForeground(Color.RED);// sPane

		centerPane.setLayout(null);
		centerPane.add(queTextArea);
		centerPane.setBackground(MyStyles.ProgramBackgroundColor);

		northPane.add(modeLabel);
		northPane.add(poKoleiBtn);
		northPane.add(losowoBtn);
		northPane.add(addCommentBtn);
		northPane.setBackground(MyStyles.panelColor);

		southPane.add(startBtn);
		southPane.add(queListBtn);
		southPane.add(sectionRankBtn);
		southPane.add(exitBtn);
		southPane.setBackground(MyStyles.panelColor);

		/*
		 * eastPane.add(learnedBtn); eastPane.setPreferredSize(new Dimension(150,500));
		 * westPane.add(unlearnedBtn); westPane.setPreferredSize(new
		 * Dimension(150,500));
		 */

		frame.getContentPane().setBackground(MyStyles.ProgramBackgroundColor);
		frame.getContentPane().add(BorderLayout.EAST, learnedBtn);
		frame.getContentPane().add(BorderLayout.WEST, unlearnedBtn);
		frame.getContentPane().add(BorderLayout.CENTER, centerPane);
		frame.getContentPane().add(BorderLayout.SOUTH, southPane);
		frame.getContentPane().add(BorderLayout.NORTH, northPane);

		// frame.getContentPane().setVisible(true);

		frame.setDefaultCloseOperation(3);// 3 to wg dokumentacji EXIT_ON_CLOSE
		frame.setSize(1200, 700);
		frame.setResizable(false);
		frame.setLocationRelativeTo(null); // centruje okno
		frame.setVisible(true);
		// przypisanie przyciskom odbiorcow
		learnedBtn.addActionListener(new LrndBtnRcvr());
		unlearnedBtn.addActionListener(new UnlrndBtnRcvr());
		queListBtn.addActionListener(new QueListRcvr());
		startBtn.addActionListener(new StartRcvr());

		exitBtn.addActionListener(new ActionListener() { // akcja wyjscia
			public void actionPerformed(ActionEvent e) {
				System.exit(0);

			}
		});
	}

	// klasy wewnetrzne bedace odbiorcami przyciskow i posiadajace metody wykonujace
	// cos w odpowiedzi na zdarzenie takie jak np klikniecie
	class LrndBtnRcvr implements ActionListener {
		public void actionPerformed(ActionEvent zdarzenie) {// event after 'learned' click
			DefaultTableModel defaultTM = (DefaultTableModel) queListWindow.queJTable.getModel();
			
			
			/*if((String)defaultTM.getValueAt(quePositionOnList, 0)==queLabel.getText()) {
				currentWasDeleted = false;
				quePositionOnList++;
			}*/
				
				
			if (start) {

				if (defaultTM.getRowCount() == 1) { //kom.1) dla przypadku, gdy na liscie jest tylko jedno pytanie
					quePositionOnList = 0;
					viewQuestion();
					showMessage();
					addPointToQuestion();
				} else {
					if (quePositionOnList > 0) { //kom.2) jeśli jest więcej, zostaje dodany punkt do
						quePositionOnList--;		// pytania obecnie wyświetlonego, czyli o pozycji
						addPointToQuestion();		// z wartoscia o 1 mniejszą niż obecnie zapisana
						quePositionOnList++;		// w quePositionOnList
					} else {
						quePositionOnList = defaultTM.getRowCount() - 1;//kom. 3) mozliwe przypadki wartosci
						addPointToQuestion();							//quePositionOnList to >0 albo rowne,
						quePositionOnList = 0;							//zatem, gdy równe wykona sie ten kod.
					}													//Chodzi o sytuacje, kiedy jestesmy na ostatnim pytaniu

					viewQuestion();
					showMessage();
					quePositionOnList++;


					if (quePositionOnList == defaultTM.getRowCount()) {//kom. 4) tu zmieniamy quePositionOnList na 0, gdy
						showMessage();									// gdy osiągniemy koniec listy. Wówczas wykona się
						viewQuestion();									//instrukcja else z komentarzem 3)
						quePositionOnList = 0;

					}
					
				}

			}else {
				showMessage();
				
			}
			
		}
	}

	class UnlrndBtnRcvr implements ActionListener {
		public void actionPerformed(ActionEvent zdarzenie) {// event after 'unlearned' click
			DefaultTableModel defaultTM = (DefaultTableModel) queListWindow.queJTable.getModel();
			if (start) {
				if (defaultTM.getRowCount() == 1) {
					quePositionOnList=0;
					showMessage();
					viewQuestion();
					
				} else {

					
					showMessage();
					viewQuestion();
					quePositionOnList++;
					if (quePositionOnList == defaultTM.getRowCount()) {
						showMessage();
						viewQuestion();
						quePositionOnList = 0;
					}
				}
			}else {
				showMessage();
			}
		}
	}

	class QueListRcvr implements ActionListener {
		public void actionPerformed(ActionEvent zdarzenie) {// event after "QueList" click
			// queLabel.setText("kliknąłeś!");

			queListWindow.queLFrame.setVisible(true);

		}
	}

	class StartRcvr implements ActionListener {
		public void actionPerformed(ActionEvent zdarzenie) {
			start = true;
			quePositionOnList=0;
			showMessage();
			viewQuestion();
			quePositionOnList++;
		}
	}

	void viewQuestion() {
		DefaultTableModel defaultTM = (DefaultTableModel) queListWindow.queJTable.getModel();

		if (defaultTM.getRowCount() > quePositionOnList) {
			queTextArea.setText((String) defaultTM.getValueAt(quePositionOnList, 0));

		}
		
	}
	void addPointToQuestion() {
		DefaultTableModel defaultTM = (DefaultTableModel) queListWindow.queJTable.getModel();
		if (defaultTM.getRowCount() > quePositionOnList && !currentWasDeleted) {
			int points = (int) defaultTM.getValueAt(quePositionOnList, 3);
			defaultTM.setValueAt(++points, quePositionOnList, 3);
		}
		currentWasDeleted = false;
	}
	void showMessage() {
		DefaultTableModel defaultTM = (DefaultTableModel) queListWindow.queJTable.getModel();
		if (!start) {
			queTextArea.setText("Wciśnij 'Start'.");
			
		}else if (defaultTM.getRowCount() == 0) {
			queTextArea.setText("Brak pytań w bazie. Dodaj lub zaimportuj nowe pytania \nw oknie 'Lista pytań'. ");
			start = false;
		}
		
	}
	

}
