package OdpytywaczJavy;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;

import javax.swing.AbstractButton;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.ScrollPaneConstants;
import javax.swing.table.DefaultTableModel;

public class QuestionListWindow {

	JFrame queLFrame;// okno listy pytań

	Object[] columnNames = { "Treść pytania", "Dział", "Komentarz", "Punkty" };
	int points;

	JTable queJTable;
	Question[] questionArray;


	JScrollPane queScrollPane;

	JButton addQueBtn;
	JButton removeQueBtn;
	JButton editQueBtn;

	JTextField queTextFieldForEditing;
	JTextField sectionField;
	JTextField commentField;

	JLabel queTextLabel;
	JLabel sectionLabel;
	JLabel commentLabel;
	JLabel messageLabel;

	public QuestionListWindow() {
		queLFrame = new JFrame("Lista pytań");

		initQueWindowComponents();

	}

	public void initQueWindowComponents() { // questionListFrame

		questionArray = null;
		queTextFieldForEditing = new JTextField("");

		addQueBtn = new JButton("Dodaj");
		removeQueBtn = new JButton("Usuń");
		editQueBtn = new JButton("Edytuj");

		queTextLabel = new JLabel("Treść pytania");
		sectionLabel = new JLabel("Dział");
		commentLabel = new JLabel("Komentarz do pytania");
		messageLabel = new JLabel();

		sectionField = new JTextField();
		commentField = new JTextField();

		addQueBtn.setBounds(50, 600, 150, 25);
		addQueBtn.setFont(MyStyles.buttonFont);
		removeQueBtn.setBounds(275, 600, 150, 25);
		removeQueBtn.setFont(MyStyles.buttonFont);
		editQueBtn.setBounds(500, 600, 150, 25);
		editQueBtn.setFont(MyStyles.buttonFont);
		queTextFieldForEditing.setBounds(50, 455, 600, 25);
		sectionField.setBounds(50, 505, 250, 25);
		commentField.setBounds(50, 555, 600, 25);
		queTextLabel.setBounds(50, 430, 100, 25);
		sectionLabel.setBounds(50, 480, 100, 25);
		commentLabel.setBounds(50, 530, 200, 25);
		queTextLabel.setFont(MyStyles.buttonFont);
		sectionLabel.setFont(MyStyles.buttonFont);
		commentLabel.setFont(MyStyles.buttonFont);
		messageLabel.setBounds(50, 640, 400, 25);
		messageLabel.setForeground(Color.RED);

		queLFrame.getContentPane().add(addQueBtn);
		queLFrame.getContentPane().add(removeQueBtn);
		queLFrame.getContentPane().add(editQueBtn);
		queLFrame.add(queTextFieldForEditing);
		queLFrame.add(sectionField);
		queLFrame.add(commentField);
		queLFrame.add(queTextLabel);
		queLFrame.add(sectionLabel);
		queLFrame.add(commentLabel);
		queLFrame.add(messageLabel);

		makeQueList();

		queLFrame.getContentPane().setLayout(null);
		queLFrame.getContentPane().setBackground(MyStyles.ProgramBackgroundColor);

		queLFrame.setSize(700, 700);
		queLFrame.setLocationRelativeTo(null);
		queLFrame.setResizable(false);

		// dodanie odbiorcy do przycisku

		addQueBtn.addActionListener(new addQueRcvr());
		removeQueBtn.addActionListener(new removeQueRcvr());
		editQueBtn.addActionListener(new editQueRcvr());
		queJTable.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				DefaultTableModel defaultTM = (DefaultTableModel) queJTable.getModel();
				if (queJTable.getSelectedRow() != -1) {
					int i = queJTable.getSelectedRow();
					queTextFieldForEditing.setText(defaultTM.getValueAt(i, 0).toString());
					sectionField.setText(defaultTM.getValueAt(i, 1).toString());
					commentField.setText(defaultTM.getValueAt(i, 2).toString());
				}
			}
		});
	}

	void makeQueList() {

		MyTableModel model = new MyTableModel();
		model.setColumnIdentifiers(columnNames);
		queJTable = new JTable();
		queJTable.setModel(model);

		queScrollPane = new JScrollPane(queJTable);
		queJTable.setFillsViewportHeight(true);

		queScrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		queScrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);


		queScrollPane.setBounds(50, 25, 600, 400);
		queLFrame.add(queScrollPane);
	}

	class addQueRcvr implements ActionListener {
		public void actionPerformed(ActionEvent zdarzenie) {// event after "dodaj" click

			DefaultTableModel defaultTM = (DefaultTableModel) queJTable.getModel();
			if (MainProgramWindow.quePositionOnList == 0) {
				MainProgramWindow.quePositionOnList = defaultTM.getRowCount();
			}
			messageLabel.setText("");

			if (!queTextFieldForEditing.getText().trim().equals("")) {
				// add question to JTable

				defaultTM.addRow(new Object[] { queTextFieldForEditing.getText(), sectionField.getText(),
						commentField.getText(), points });

				// clear fields
				queTextFieldForEditing.setText("");
				queTextFieldForEditing.requestFocusInWindow();
				sectionField.setText("");
				commentField.setText("");
			} else {
				queTextFieldForEditing.requestFocusInWindow();
				messageLabel.setText("To pole nie może być puste!");
			}

		}
	}

	class removeQueRcvr implements ActionListener {
		public void actionPerformed(ActionEvent zdarzenie) {
			DefaultTableModel defaultTM = (DefaultTableModel) queJTable.getModel();
			
			System.out.println(isInTable("3") + " " + getIndex("3"));
			
			String queTextAreaCopy = null;
			if (defaultTM.getRowCount() > 0) {
				if (MainProgramWindow.quePositionOnList == 0) {
					queTextAreaCopy = (String) defaultTM.getValueAt
							(defaultTM.getRowCount() - 1, 0);
				} else {
					queTextAreaCopy = (String) defaultTM.getValueAt
							(MainProgramWindow.quePositionOnList - 1, 0);// kopia pytania wyswietlonego przed usunieciem
																						//by sprawdzic potem, czy usunięto ostatnio wyswietlone pytanie
				}
			}

			int[] i = queJTable.getSelectedRows();
			if (i.length > 0) {


				for (int k = i.length; k > 0; k--) {
					defaultTM.removeRow(i[k - 1]);

				}

				// clear fields
				queTextFieldForEditing.setText("");
				queTextFieldForEditing.requestFocusInWindow();
				sectionField.setText("");
				commentField.setText("");

				if (isInTable(queTextAreaCopy)) { //rozwiazanie z obecnym isInTable() nieodporne na powtorzone pytania. Gdy bedzie baza danych trzeba bedzie w tej metodzie sprawdzac unikalne id pytania, a nie jego tekst.
					MainProgramWindow.quePositionOnList = getIndex(queTextAreaCopy)+1;
					System.out.println("id "+MainProgramWindow.quePositionOnList);
				}else{
					MainProgramWindow.quePositionOnList = 0;//wróci do pytania pierwszego
					MainProgramWindow.currentWasDeleted = true; //dzięki tej fladze, po usunieciu pytania obecnie wyswietlonego, nie zostanie dodny punkt do pytania przed nim
					System.out.println("flaga:"+MainProgramWindow.currentWasDeleted);
				}

				/*
				 * if((String)defaultTM.getValueAt(MainProgramWindow.quePositionOnList, 0)=="")
				 * {//zmienic tu waruek na taki zeby sprwdza;l
				 * MainProgramWindow.currentWasDeleted = true; }
				 */

			} else {
				messageLabel.setText("Zaznacz wiersz do usunięcia.");

			}

			if (defaultTM.getRowCount() == 0) {
				MainProgramWindow.start = false;
			}
			System.out.println("Obecna pozycja: "+MainProgramWindow.quePositionOnList);
		}

	}

	class editQueRcvr implements ActionListener {
		public void actionPerformed(ActionEvent zdarzenie) {
			DefaultTableModel defaultTM = (DefaultTableModel) queJTable.getModel();

			int i = queJTable.getSelectedRow();
			if (i >= 0) {
				messageLabel.setText("");
				defaultTM.setValueAt(queTextFieldForEditing.getText(), i, 0);
				defaultTM.setValueAt(sectionField.getText(), i, 1);
				defaultTM.setValueAt(commentField.getText(), i, 2);

			} else {
				messageLabel.setText("Zaznacz wiersz do edycji.");

			}

		}

	}

	boolean isInTable(String questionText) {

		DefaultTableModel defaultTM = (DefaultTableModel) queJTable.getModel();
		int k = defaultTM.getRowCount() - 1;
		boolean is = false;
		for (int i = k; i >= 0; i--) {
			if (defaultTM.getValueAt(i, 0).equals(questionText)) {
				is = true;
			}
		}
		return is;
	}

	int getIndex(String v) {
		DefaultTableModel defaultTM = (DefaultTableModel) queJTable.getModel();
		int k = defaultTM.getRowCount() - 1;
		int id = -1;
		for (int i = k; i >= 0; i--) {
			if (defaultTM.getValueAt(i, 0).equals(v)) {
				id = i;
			}
		}
		return id;
	}

}
