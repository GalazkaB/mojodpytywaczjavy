package OdpytywaczJavy;

import java.awt.Color;
import java.awt.Font;

public class MyStyles {
	public static Color queLettersColor = new Color(21, 67, 96);
	public static Color panelColor = new Color(175, 238, 238);
	public static Color ProgramBackgroundColor = new Color(120, 228, 215);
	public static Font queFont = new Font("czcionka", Font.BOLD, 25);
	public static Font queFontOnQueList = new Font("czcionkaListyPytań", Font.ITALIC, 18);
	public static Font buttonFont = new Font("czcionka2", Font.BOLD, 15);
}
